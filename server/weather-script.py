#!/usr/bin/env python2

# Kindle Weather Display
# Matthew Petroff (http://mpetroff.net/)
# September 2012
from xml.dom import minidom
import datetime
import time
import codecs
import os
import logging
from subprocess import *
from requests import get
from shutil import copy
try:
    # Python 3
    from urllib.request import urlopen, URLError
except ImportError:
    # Python 2
    from urllib2 import urlopen, URLError
    
FULL_LATITUDE = 35.3225752
FULL_LONGITUDE = -80.7048082
#LATITUDE = round(FULL_LATITUDE, 4)
#LONGITUDE = round(FULL_LONGITUDE, 4)
LATITUDE = FULL_LATITUDE
LONGITUDE = FULL_LONGITUDE
GEO_URL = 'http://freegeoip.net/json'
LAST_HIGHS = 0
LAST_LOWS = 0
LAST_ICONS = None
URL_BASE = 'https://home-wisehub.pagekite.me/api/states'
HEADERS = {'x-ha-access': 'CbCa7x898p', 'content-type': 'application/json'}
SCRIPT_DIR = os.getcwd()

logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter_ = logging.Formatter(
    '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter_)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


def fetch_weather_data(latitude, longitude):
    logger.debug('fetching weather')
    weather_xml = urlopen('http://graphical.weather.gov/xml/SOAP_server/ndfdSOAPclientByDay.php?whichClient=NDFDgenByDay&lat=' + str(latitude) + '&lon=' + str(longitude) + '&format=24+hourly&numDays=4&Unit=e').read()
    return minidom.parseString(weather_xml)


def parse_temperatures(dom):
    logger.debug('parsing temperature')
    xml_temperatures = dom.getElementsByTagName('temperature')
    highs = [None]*4
    lows = [None]*4
    for item in xml_temperatures:
        if item.getAttribute('type') == 'maximum':
            values = item.getElementsByTagName('value')
            for i in range(len(values)):
                highs[i] = int(values[i].firstChild.nodeValue)
        if item.getAttribute('type') == 'minimum':
            values = item.getElementsByTagName('value')
            for i in range(len(values)):
                lows[i] = int(values[i].firstChild.nodeValue)
                
    return highs, lows


def parse_icons(dom):
    logger.debug('parsing icons')
    xml_icons = dom.getElementsByTagName('icon-link')
    icons = [None]*4
    for i in range(len(xml_icons)):
        icons[i] = xml_icons[i].firstChild.nodeValue.split('/')[-1].split('.')[0].rstrip('0123456789')
        
    return icons


def parse_dates(dom):
    logger.debug('parsing dates')
    xml_day_one = dom.getElementsByTagName('start-valid-time')[0].firstChild.nodeValue[0:10]
    return datetime.datetime.strptime(xml_day_one, '%Y-%m-%d')


def process_svg(highs, lows, icons, include_forecast=False):
    logger.debug('processing svg')
    output = codecs.open('{}/preprocess.svg'.format(SCRIPT_DIR), 'r', encoding='utf-8').read()
    # Insert icons and temperatures
    output = output.replace('WEATHER_ICON',icons[0])
    output = output.replace('HIGH',str(highs[0])).replace('LOW',str(lows[0]))

    output = output.replace('BEDROOM_TEMP', str(get_remote_arduino_temp())).replace('BEDROOM_HUM', str(get_remote_arduino_humid()))
    output = output.replace('FED_TIME', str(get_fed_time()))

    # Set Clock
    logger.debug('setting clock')
    output = output.replace('CLOCK',time.strftime('%I:%M %p', time.localtime()).lstrip('0'))

    # Write output
    codecs.open('{}/weather-script-output.svg'.format(SCRIPT_DIR), 'w', encoding='utf-8').write(output)


def run_cmd(cmd):
    process = Popen(cmd.split(), stdout=PIPE)
    output = process.communicate()[0]
    return output


def create_png(include_forecast=False):
    logger.debug('creating png')
    global LAST_HIGHS, LAST_LOWS, LAST_ICONS
    if include_forecast:
        geolocation = get(GEO_URL).json()
        latitude = geolocation.get('latitude')
        longitude = geolocation.get('longitude')
        weather_data = fetch_weather_data(latitude, longitude)
        LAST_HIGHS,LAST_LOWS = highs,lows = parse_temperatures(weather_data)
        LAST_ICONS = icons = parse_icons(weather_data)
        day_one = parse_dates(weather_data)
    else:
        highs,lows,icons= LAST_HIGHS,LAST_LOWS,LAST_ICONS
        
    process_svg(highs, lows, icons, include_forecast)
    
    if os.name == 'posix':
        logger.debug('running rsvg')
        run_cmd("rsvg-convert --background-color=white -o weather-script.png weather-script-output.svg")
        # PNG Crush is necessary to have image format properly on Kindle
        run_cmd("pngcrush -c 0 -ow weather-script.png")
        copy("weather-script.png", "/home/wisdomwolf/.docker/nginx/html/images/weather-script-output.png")
    
    print("Uploading png. {0}".format(time.strftime("%c", time.localtime())))
    

def get_pi_temp():
    return 74
    

def get_remote_arduino_temp():
    try:
        return int(get_ha_data('sensor.bedroom_temperature'))
    except ValueError:
        return 'ERR'
    except:
        return 'N/A'


def get_remote_arduino_humid():
    try:
        return int(get_ha_data('sensor.bedroom_humidity'))
    except ValueError:
        return 'ERR'
    except:
        return 'N/A'

    
def get_fed_time():
    try:
        return get_ha_data('sensor.dog_food')
    except ValueError:
        return 'ERR'
    except:
        return 'N/A'


def get_ha_data(state, url_base=None, headers=None):
    url_base = url_base or URL_BASE
    headers = headers or HEADERS
    url = '{}/{}'.format(url_base, state)
    response = get(url, headers=headers)
    return response.json()['state']


def wait_for_next_minute(current=time.strftime('%M', time.localtime())):
    while True:
        if time.strftime('%M', time.localtime()) == current:
            continue
        else:
            create_png(True)
            efficient_time_update()
            break


def efficient_time_update():
    while True:
        time.sleep(56)
        this_minute = time.strftime('%M', time.localtime()) 
        if this_minute == 0:
            create_png(True)
            wait_for_next_minute()
            break
        else:
            create_png(False)

def main():    
	create_png(True)
	wait_for_next_minute()

if __name__ == "__main__":
	main()
    
